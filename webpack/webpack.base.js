const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = mode => ({
    devtool: 'source-map',
    entry: {
        app: './app'
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        publicPath: './dist/',
        filename: '[name].js'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['node_modules', path.resolve(__dirname, '../app')]
    },
    module: {
        rules: [
            {
                test: /\.js?x$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    highlightCode: true
                }
            },
            {
                test: /\.(png|svg|jpe?g|gif)$/,
                exclude: /svg[\/\\]/,
                loader: 'file-loader',
                options: {
                    name: 'images/[name].[ext]'
                }
            },
            {
                test: /\.(woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'fonts',
                    publicPath: './fonts'
                }
            },
            {
                test: /\.svg$/,
                include: /svg[\/\\]/,
                use: [
                    {
                        loader: 'svg-sprite-loader',
                        options: {
                            symbolId: 'icon-[name]'
                        }
                    },
                    {
                        loader: 'svgo-loader',
                        options: {
                            plugins: [
                                { removeNonInheritableGroupAttrs: true },
                                { collapseGroups: true },
                                { removeAttrs: { attrs: '(fill|stroke)' } }
                            ]
                        }
                    }
                ]
            }
        ]
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            automaticNameDelimiter: '.',
            name: 'vendors'
        },
        runtimeChunk: {
            name: 'vendors'
        }
    },
    plugins: [new CleanWebpackPlugin()],
    stats: {
        children: false
    }
});
