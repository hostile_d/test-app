import App from './general/js/app';

import normalize from 'normalize.css';
import './general/scss/index.scss';

const app = new App();

const files = require.context('./general/svg', true, /^\.\/.*\.svg/);
files.keys().forEach(files);
const spriteNode = document.getElementById('__SVG_SPRITE_NODE__');

if (spriteNode) {
    spriteNode.setAttribute('focusable', 'false');
    spriteNode.setAttribute('aria-hidden', 'true');
}
