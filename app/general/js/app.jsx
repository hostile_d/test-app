import React from 'react';
import ReactDOM from 'react-dom';
import Container from '../../components/container';
import Sidebar from '../../components/sidebar';
import Content from '../../components/content';

const ROOT_ELEMENT_ID = 'root';

export default class App {
    constructor() {
        this.init();
    }
    init() {
        const rootElement = document.getElementById(ROOT_ELEMENT_ID);
        ReactDOM.render(this.container(), rootElement);
    }
    container() {
        return (
            <Container>
                <Sidebar />
                <Content />
            </Container>
        );
    }
}
