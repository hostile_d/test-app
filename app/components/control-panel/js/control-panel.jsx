import React from 'react';
import 'react-tabs/style/react-tabs.scss';
import Tabs from '../../tabs';

export default class ControlPanel extends React.Component {
    render() {
        return (
            <form className="control-panel">
                <Tabs />
                <div className="control-panel__footer control-panel-footer">
                    <button
                        className="control-panel-footer__submit-btn"
                        type="submit"
                    >
                        SAVE CHANGES
                    </button>
                    <button
                        className="control-panel-footer__cancel-btn"
                        type="submit"
                    >
                        Cancel
                    </button>
                    <div className="control-panel-footer__copy">
                        to reset changes
                    </div>
                </div>
            </form>
        );
    }
}
