import React from 'react';
import PropTypes from 'prop-types';

const DEFAULT_CSS_CLASS = 'icon';

export default class Icon extends React.Component {
    getSVGId() {
        const { iconName } = this.props;
        return `#icon-${iconName}`;
    }
    getCSSClass() {
        const { CSSClass } = this.props;
        return `${DEFAULT_CSS_CLASS} ${CSSClass ? CSSClass : ''}`;
    }
    render() {
        return (
            <svg
                width="26"
                height="26"
                className={this.getCSSClass()}
                focusable="false"
                role="image"
                aria-hidden="true"
            >
                <use xlinkHref={this.getSVGId()} />
            </svg>
        );
    }
}
Icon.propTypes = {
    iconName: PropTypes.string.isRequired,
    CSSClass: PropTypes.string.isRequired
};
