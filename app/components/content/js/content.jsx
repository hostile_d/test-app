import React from 'react';
import ControlPanel from '../../control-panel';

export default class Content extends React.Component {
    render() {
        return (
            <div className="content">
                <h1 className="content__title">Settings</h1>
                <ControlPanel />
            </div>
        );
    }
}
