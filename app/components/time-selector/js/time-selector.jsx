import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import TimePicker from 'react-time-picker';

const MIN_VALUE = 0;
const MAX_VALUE = 24;
const TIME_FORMAT = 'HH:mm';
export default class TimeSelector extends React.Component {
    state = {
        beginTime: this.props.beginTime,
        endTime: this.props.endTime
    };
    componentWillReceiveProps(nextProps) {
        const beginTime = nextProps.beginTime;
        const endTime = nextProps.endTime;
        this.setState({ beginTime, endTime });
    }
    onChangeMin = data => {
        const beginTime = this.parseTimeInputValue(data);
        this.setState({
            beginTime
        });
        if (this.props.onWholeWeekChange) {
            this.props.onWholeWeekChange({ beginTime });
        }
    };
    onChangeMax = data => {
        const endTime = this.parseTimeInputValue(data);
        this.setState({
            endTime
        });
        if (this.props.onWholeWeekChange) {
            this.props.onWholeWeekChange({ endTime });
        }
    };
    onSliderChange = data => {
        const beginTime = this.formatToString(data[0]);
        const endTime = this.formatToString(data[1]);

        this.setState({
            beginTime,
            endTime
        });
        if (this.props.onWholeWeekChange) {
            this.props.onWholeWeekChange({
                beginTime,
                endTime
            });
        }
    };
    parseTimeInputValue(value) {
        if (value && typeof value !== 'string') {
            return;
        }

        const hours = parseInt(value.split(':')[0], 10);
        const minutes = parseInt(value.split(':')[1], 10);

        return `${this.getTwoSymbols(hours)}:${this.getTwoSymbols(minutes)}`;
    }
    formatToString(sliderValue) {
        if (sliderValue && typeof sliderValue !== 'number') {
            return;
        }
        if (sliderValue <= 0 || sliderValue >= 24) {
            return '00:00';
        }
        return `${this.getTwoSymbols(sliderValue)}:00`;
    }
    formatToNumber(value) {
        if (value && typeof value !== 'string') {
            return;
        }
        return parseInt(value.split(':')[0], 10);
    }
    getTwoSymbols(digit) {
        if (digit < 10) {
            return `0${digit}`;
        } else {
            return `${digit}`;
        }
    }

    render() {
        return (
            <div className="time-selector">
                <div className="time-selector__inputs">
                    <TimePicker
                        className="time-selector__input"
                        onChange={this.onChangeMin}
                        value={this.state.beginTime}
                        disableClock={true}
                        format={TIME_FORMAT}
                    />
                    <div className="time-selector__separator">-</div>
                    <TimePicker
                        className="time-selector__input"
                        onChange={this.onChangeMax}
                        value={this.state.endTime}
                        disableClock={true}
                        format={TIME_FORMAT}
                    />
                </div>
                <Range
                    className="time-selector__range"
                    min={MIN_VALUE}
                    max={MAX_VALUE}
                    onChange={this.onSliderChange}
                    allowCross={false}
                    pushable={true}
                    value={[
                        this.formatToNumber(this.state.beginTime),
                        this.formatToNumber(this.state.endTime)
                    ]}
                />
            </div>
        );
    }
}

TimeSelector.propTypes = {
    beginTime: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
    onWholeWeekChange: PropTypes.func
};
