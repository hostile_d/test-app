import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import TimeSelector from '../../time-selector/';
import Icon from '../../icon/';

const DEFAULT_BEGIN_TIME = '08:00';
const DEFAULT_END_TIME = `17:00`;
export default class Week extends React.Component {
    state = {
        weekBeginTime: DEFAULT_BEGIN_TIME,
        weekEndTime: DEFAULT_END_TIME,
        week: [
            {
                title: 'Working Hours',
                isActive: true,
                withCheckbox: false,
                wholeWeekSelector: true,
                beginTime: DEFAULT_BEGIN_TIME,
                endTime: DEFAULT_END_TIME
            },
            {
                title: 'Monday',
                isActive: true,
                withCheckbox: true,
                beginTime: DEFAULT_BEGIN_TIME,
                endTime: DEFAULT_END_TIME
            },
            {
                title: 'Tuesday',
                isActive: true,
                withCheckbox: true,
                beginTime: DEFAULT_BEGIN_TIME,
                endTime: DEFAULT_END_TIME
            },
            {
                title: 'Wednesday',
                isActive: true,
                withCheckbox: true,
                beginTime: DEFAULT_BEGIN_TIME,
                endTime: DEFAULT_END_TIME
            },
            {
                title: 'Thursday',
                isActive: true,
                withCheckbox: true,
                beginTime: DEFAULT_BEGIN_TIME,
                endTime: DEFAULT_END_TIME
            },
            {
                title: 'Friday',
                isActive: true,
                withCheckbox: true,
                beginTime: DEFAULT_BEGIN_TIME,
                endTime: DEFAULT_END_TIME
            },
            {
                title: 'Saturday',
                isActive: false,
                withCheckbox: true,
                beginTime: DEFAULT_BEGIN_TIME,
                endTime: DEFAULT_END_TIME
            },
            {
                title: 'Sunday',
                isActive: false,
                withCheckbox: true,
                beginTime: DEFAULT_BEGIN_TIME,
                endTime: DEFAULT_END_TIME
            }
        ]
    };

    onWholeWeekChange = data => {
        const weekBeginTime = data.beginTime;
        const weekEndTime = data.endTime;
        let week = this.state.week;
        if (weekBeginTime) {
            week.map((weekday, i) => (weekday.beginTime = weekBeginTime));
        }
        if (weekEndTime) {
            week.map((weekday, i) => (weekday.endTime = weekEndTime));
        }
        this.setState(week);
    };
    getTimeSelectorCSSClass(isActive) {
        return isActive ? '' : ' week__time-selector--hidden';
    }
    onCheckboxToggle = dayIndex => {
        const isActive = this.state.week[dayIndex].isActive ? false : true;
        const week = this.state.week;
        week[dayIndex].isActive = isActive;
        this.setState({ week });
    };
    render() {
        return (
            <div className="week">
                {this.state.week.map((weekday, i) => (
                    <div className="week__day" key={i}>
                        <div className="week__day-title">
                            {weekday.withCheckbox ? (
                                <React.Fragment>
                                    <input
                                        type="checkbox"
                                        className="week__input"
                                        defaultChecked={
                                            this.state.week[i].isActive
                                        }
                                        onChange={this.onCheckboxToggle.bind(
                                            this,
                                            i
                                        )}
                                        id={i}
                                    />
                                    <label className="week__label" htmlFor={i}>
                                        <Icon
                                            CSSClass="week__tick"
                                            iconName="tick"
                                        />
                                        {weekday.title}
                                    </label>
                                </React.Fragment>
                            ) : (
                                <div>{weekday.title}</div>
                            )}
                        </div>
                        <div
                            className={`week__time-selector${this.getTimeSelectorCSSClass(
                                weekday.isActive
                            )}`}
                        >
                            {weekday.wholeWeekSelector ? (
                                <TimeSelector
                                    beginTime={weekday.beginTime}
                                    endTime={weekday.endTime}
                                    onWholeWeekChange={this.onWholeWeekChange}
                                />
                            ) : (
                                <TimeSelector
                                    beginTime={weekday.beginTime}
                                    endTime={weekday.endTime}
                                />
                            )}
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}

Week.defaultProps = {};

Week.propTypes = {};
