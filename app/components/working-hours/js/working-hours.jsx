import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import Flextime from '../../flextime/';
import Week from '../../week/';
export default class WorkingHours extends React.Component {
    render() {
        return (
            <div className="working-hours">
                <Flextime />
                <Week />
            </div>
        );
    }
}

WorkingHours.defaultProps = {
    list: []
};

WorkingHours.propTypes = {
    list: PropTypes.array.isRequired
};
