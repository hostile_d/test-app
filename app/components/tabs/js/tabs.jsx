import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import WorkingHours from '../../working-hours/';

export default class TabsComponent extends React.Component {
    getDefaultIndex() {
        let defaultIndex = 0;
        this.props.tabs.filter((tab, index) => {
            if (tab.content !== null) {
                defaultIndex = index;
            }
        });
        return defaultIndex;
    }
    render() {
        return (
            <Tabs className="tabs" defaultIndex={this.getDefaultIndex()}>
                <TabList className="tabs__list">
                    {this.props.tabs.map(tab => (
                        <Tab
                            className="tabs__tab"
                            selectedClassName="tabs__tab--selected"
                            key={shortid.generate()}
                        >
                            {tab.title}
                        </Tab>
                    ))}
                </TabList>
                {this.props.tabs.map(tab => (
                    <TabPanel
                        className="tabs__panel"
                        selectedClassName="tabs__panel--selected"
                        key={shortid.generate()}
                    >
                        {tab.content}
                    </TabPanel>
                ))}
            </Tabs>
        );
    }
}

TabsComponent.defaultProps = {
    tabs: [
        { title: 'Profile', content: null },
        { title: 'Notifications', content: null },
        { title: 'Password', content: null },
        { title: 'Working hours', content: <WorkingHours /> },
        { title: 'Call settings', content: null }
    ]
};

TabsComponent.propTypes = {
    tabs: PropTypes.array.isRequired
};
