import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';

export default class Flextime extends React.Component {
    render() {
        return (
            <div className="flextime">
                <div className="flextime__title">Schedule</div>
                {this.props.buttons.map(button => (
                    <div className="flextime__radio" key={shortid.generate()}>
                        <input
                            className="flextime__input"
                            name="flextime-radio"
                            type="radio"
                            id={button.id}
                        />
                        <label className="flextime__label" htmlFor={button.id}>
                            {button.title}
                        </label>
                    </div>
                ))}
            </div>
        );
    }
}

Flextime.defaultProps = {
    buttons: [
        {
            id: 'fixed',
            title: 'Fixed'
        },
        {
            id: 'flexible',
            title: 'Flexible'
        }
    ]
};

Flextime.propTypes = {
    buttons: PropTypes.array.isRequired
};
