import React from 'react';
import Icon from '../../icon';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import defaultUserPic from '../../../general/img/userPic.png';

export default class Sidebar extends React.Component {
    render() {
        return (
            <aside className="sidebar">
                <ul className="sidebar__list">
                    {this.props.list.map(item => (
                        <li
                            className="sidebar__list-item"
                            key={shortid.generate()}
                        >
                            <a
                                className="sidebar__link"
                                href={item.href || '#'}
                            >
                                <div className="sidebar__icon-wrapper">
                                    <Icon
                                        CSSClass="sidebar__icon"
                                        iconName={item.icon}
                                    />
                                </div>
                                {item.name}
                            </a>
                        </li>
                    ))}
                </ul>
                <div className="sidebar__footer">
                    <div className="sidebar__footer-userpic">
                        <img src={defaultUserPic} alt="User Picture" />
                    </div>
                    <div className="sidebar__footer-text">Settings</div>
                </div>
            </aside>
        );
    }
}

Sidebar.defaultProps = {
    list: [
        { name: 'Dashboard', icon: 'clock' },
        { name: 'Customers', icon: 'card' },
        { name: 'Calendar', icon: 'calendar' },
        { name: 'Chats', icon: 'list' },
        { name: 'Tasks', icon: 'clock' },
        { name: 'Calls', icon: 'inbox' },
        { name: 'Dashboard', icon: 'phone' },
        { name: 'Leads', icon: 'folder' },
        { name: 'Team', icon: 'couple' }
    ]
};

Sidebar.propTypes = {
    list: PropTypes.array.isRequired
};
